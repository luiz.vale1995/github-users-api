import { ProgrammingLanguageEntity } from "db/entities/programming.language.entity";
import { Read } from "./read";

export interface ReadProgrammingLanguageRepository
  extends Read<ProgrammingLanguageEntity> {}
