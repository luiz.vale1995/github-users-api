import { UserEntity } from "@/db/entities";
import { Read } from "./read";

export interface ReadUserRepository extends Read<UserEntity> {
  findByTeam(teamId: string): Promise<UserEntity[]>;
}
