import { TeamEntity } from "@/db/entities";
import { Read } from "./read";

export interface ReadTeamRepository extends Read<TeamEntity> {
  findByProgrammingLanguage(languageId: string): Promise<TeamEntity[]>;
}
