export interface Read<T> {
  find(): Promise<T[]>;
  findOne(id: string): Promise<T>;
}
