import DataBaseConfiguration from "@/db/config/data.source";
import { UserEntity } from "@/db/entities";
import BaseRepository from "./base.repository";
import { ReadUserRepository } from "./interfaces/read.user.repository";

export class UserRepository
  extends BaseRepository<UserEntity>
  implements ReadUserRepository
{
  constructor() {
    super(DataBaseConfiguration, UserEntity);
  }

  async findByTeam(teamId: string): Promise<UserEntity[]> {
    const users = this.db
      .createQueryBuilder("user")
      .leftJoinAndSelect("user.teams", "team")
      .where("team.id=:teamId", { teamId })
      .getMany();
    return users;
  }

  async findOne(id: string): Promise<UserEntity> {
    const user = await this.db.findOneBy({
      id: id,
    });
    return user;
  }

  async find(): Promise<UserEntity[]> {
    return await this.db.find();
  }
}
