import BaseRepository from "./base.repository";
import { ProgrammingLanguageEntity } from "@/db/entities";
import DataBaseConfiguration from "@/db/config/data.source";
import { ReadProgrammingLanguageRepository } from "./interfaces";

export class ProgrammingLanguageRepository
  extends BaseRepository<ProgrammingLanguageEntity>
  implements ReadProgrammingLanguageRepository
{
  constructor() {
    super(DataBaseConfiguration, ProgrammingLanguageEntity);
  }
  async findOne(id: string): Promise<ProgrammingLanguageEntity> {
    return await this.db.findOneBy({
      id: id,
    });
  }

  async find(): Promise<ProgrammingLanguageEntity[]> {
    return await this.db.find();
  }
}
