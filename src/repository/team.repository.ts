import DataBaseConfiguration from "@/db/config/data.source";
import { TeamEntity } from "@/db/entities";
import BaseRepository from "./base.repository";
import { ReadTeamRepository } from "./interfaces/read.team.repository";

export class TeamRepository
  extends BaseRepository<TeamEntity>
  implements ReadTeamRepository
{
  constructor() {
    super(DataBaseConfiguration, TeamEntity);
  }

  async findByProgrammingLanguage(languageId: string): Promise<TeamEntity[]> {
    const teams = this.db
      .createQueryBuilder("team")
      .leftJoinAndSelect("team.programmingLanguages", "programming_language")
      .where("programming_language.id=:languageId", { languageId })
      .getMany();
    return teams;
  }

  async findOne(id: string): Promise<TeamEntity> {
    return await this.db.findOneBy({
      id: id,
    });
  }
  async find(): Promise<TeamEntity[]> {
    return await this.db.find();
  }
}
