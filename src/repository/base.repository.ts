import { DataSource, EntityTarget, Repository } from "typeorm";
import { Read } from "./interfaces";

export default abstract class BaseRepository<T> implements Read<T> {
  protected readonly db: Repository<T>;

  constructor(datasource: DataSource, entity: EntityTarget<T>) {
    this.db = datasource.getRepository(entity);
  }

  find(): Promise<T[]> {
    throw new Error("Method not implemented.");
  }
  findOne(id: string): Promise<T> {
    throw new Error("Method not implemented.");
  }
}
