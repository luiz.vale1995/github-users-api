export default {
  githubUrl: process.env.GITHUB_URL || "https://api.github.com/users",
  port: process.env.PORT || 5050,
};

export const envDb = {
  name: process.env.DB_NAME || "db",
  password: process.env.DB_PASS || "pass",
  user: process.env.DB_USER || "user",
  port: 5432,
  host: process.env.POSTGRES_URL || "localhost",
};
