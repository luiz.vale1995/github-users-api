import {
  ProgrammingLanguageController,
  TeamController,
  UserController,
} from "@/controllers";
import {
  ProgrammingLanguageRepository,
  TeamRepository,
  UserRepository,
} from "@/repository";
import {
  ProgrammingLanguageService,
  TeamService,
  UserService,
} from "@/services";

//Resolve modules to User Controller
const userRepo = new UserRepository();
const userService = new UserService(userRepo);

// Inject user service into the controller to create it
const createUserController = new UserController(userService);

// Resolve modules to Team Controller
const teamRepo = new TeamRepository();
const teamService = new TeamService(teamRepo);

// Inject team service into the controller to create it
const createTeamController = new TeamController(teamService);

// Resolve modules to Programming Language Controller
const programmingLanguageRepository = new ProgrammingLanguageRepository();
const programmingLanguageService = new ProgrammingLanguageService(
  programmingLanguageRepository
);
// Inject programming language service into the controller to create it
const createProgrammingLanguageController = new ProgrammingLanguageController(
  programmingLanguageService
);

export {
  createUserController,
  createTeamController,
  createProgrammingLanguageController,
};
