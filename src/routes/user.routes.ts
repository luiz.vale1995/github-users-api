import { Router } from "express";
import { createUserController } from "@/config/module";
export default (router: Router): void => {
  router.get("/users", (req, res) => createUserController.getAll(req, res));
  router.get("/user/:id", (req, res) => createUserController.get(req, res));
  router.get("/user/:teamId/users", (req, res) =>
    createUserController.getByTeamId(req, res)
  );
};
