import { Router } from "express";
import { createProgrammingLanguageController } from "@/config/module";

export default (router: Router): void => {
  router.get("/programming-languages", (req, res) =>
    createProgrammingLanguageController.getAll(req, res)
  );
  router.get("/programming-language/:id", (req, res) =>
    createProgrammingLanguageController.get(req, res)
  );
};
