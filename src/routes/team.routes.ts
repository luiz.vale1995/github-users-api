import { Router } from "express";
import { createTeamController } from "@/config/module";

export default (router: Router): void => {
  router.get("/teams", (req, res) => createTeamController.getAll(req, res));
  router.get("/team/:id", (req, res) => createTeamController.get(req, res));
  router.get("/team/:languageId/teams", (req, res) =>
    createTeamController.getByProgrammingLanguageId(req, res)
  );
};
