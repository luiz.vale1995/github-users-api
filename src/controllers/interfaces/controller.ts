import { Request, Response } from "express";

export interface Controller {
  get(req: Request, res: Response): void;
  getAll(req: Request, res: Response): void;
}
