import { Request, Response } from "express";
import { User } from "@/models/user";
import { Controller } from "./interfaces";
import { ReadUserService } from "@/services/interfaces";
import Message from "@/utils/message";

export class UserController implements Controller {
  private userService: ReadUserService;

  constructor(userService: ReadUserService) {
    this.userService = userService;
  }

  async get(req: Request, res: Response): Promise<void> {
    try {
      const user: User = await this.userService.get(req.params.id);
      if (!user) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(user);
      }
    } catch (error) {
      console.log(error);
      res.status(500).send(Message.InternalError);
    }
  }

  async getAll(req: Request, res: Response): Promise<void> {
    try {
      const users: User[] = await this.userService.getAll();
      if (users.length === 0) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(users);
      }
    } catch (error) {
      res.status(500).send(Message.InternalError);
    }
  }

  async getByTeamId(req: Request, res: Response): Promise<void> {
    try {
      const users: User[] = await this.userService.getByTeamId(
        req.params.teamId
      );
      if (users.length === 0) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(users);
      }
    } catch (error) {
      res.status(500).send(Message.InternalError);
    }
  }
}
