import { Request, Response } from "express";
import { ProgrammingLanguage } from "@/models";
import { Controller } from "./interfaces";
import { ReadProgrammingLanguageService } from "@/services/interfaces";
import Message from "@/utils/message";

export class ProgrammingLanguageController implements Controller {
  private programmingLanguageService: ReadProgrammingLanguageService;

  constructor(programmingLanguageService: ReadProgrammingLanguageService) {
    this.programmingLanguageService = programmingLanguageService;
  }

  async get(req: Request, res: Response): Promise<void> {
    try {
      const programmingLanguage: ProgrammingLanguage =
        await this.programmingLanguageService.get(req.params.id);
      if (!programmingLanguage) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(programmingLanguage);
      }
    } catch (error) {
      res.status(500).send(Message.InternalError);
    }
  }

  async getAll(req: Request, res: Response): Promise<void> {
    try {
      const programmingLanguages: ProgrammingLanguage[] =
        await this.programmingLanguageService.getList();
      if (programmingLanguages.length === 0) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(programmingLanguages);
      }
    } catch (error) {
      res.status(500).send(Message.InternalError);
    }
  }
}
