import { Request, Response } from "express";
import { Team } from "@/models";
import { Controller } from "./interfaces";
import { ReadTeamService } from "@/services/interfaces";
import Message from "@/utils/message";

export class TeamController implements Controller {
  private teamService: ReadTeamService;

  constructor(teamService: ReadTeamService) {
    this.teamService = teamService;
  }

  async get(req: Request, res: Response): Promise<void> {
    try {
      const team: Team = await this.teamService.get(req.params.id);
      if (!team) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(team);
      }
    } catch (error) {
      res.status(500).send(Message.InternalError);
    }
  }

  async getAll(req: Request, res: Response): Promise<void> {
    try {
      const teams: Team[] = await this.teamService.getAll();
      if (teams.length === 0) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(teams);
      }
    } catch (error) {
      res.status(500).send(Message.InternalError);
    }
  }

  async getByProgrammingLanguageId(req: Request, res: Response): Promise<void> {
    try {
      const teams: Team[] = await this.teamService.getByProgrammingLanguageId(
        req.params.languageId
      );
      if (teams.length === 0) {
        res.status(404).send(Message.NotFound);
      } else {
        res.status(200).send(teams);
      }
    } catch (error) {
      res.status(500).send(Message.InternalError);
    }
  }
}
