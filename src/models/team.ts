import { Base } from "@/models/base";

export interface Team extends Base {
  name: string;
}
