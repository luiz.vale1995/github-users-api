import { Base } from "@/models/base";

export interface ProgrammingLanguage extends Base {
  name: string;
}
