import { Team } from "@/models";
import { TeamRepository } from "@/repository";
import { ReadTeamRepository } from "@/repository/interfaces/read.team.repository";
import { ReadTeamService } from "./interfaces";

export class TeamService implements ReadTeamService {
  private teamRepository: TeamRepository;

  constructor(teamRepository: TeamRepository) {
    this.teamRepository = teamRepository;
  }
  async get(id: string): Promise<Team> {
    try {
      return await this.teamRepository.findOne(id);
    } catch (e) {
      console.error(
        `Error in TeamService method get with error message: ${e.message}`
      );
    }
  }
  async getAll(): Promise<Team[]> {
    try {
      return await this.teamRepository.find();
    } catch (e) {
      console.error(
        `Error in TeamService method getList with error message: ${e.message}`
      );
    }
  }

  async getByProgrammingLanguageId(languageId: string): Promise<Team[]> {
    try {
      return await this.teamRepository.findByProgrammingLanguage(languageId);
    } catch (e) {
      console.error(
        `Error in TeamService method getTeamByProgramingLanguage with error message: ${e.message}`
      );
    }
  }
}
