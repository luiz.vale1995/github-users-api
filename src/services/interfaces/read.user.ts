import { User } from "@/models";

export interface ReadUserService {
  get: (name: string) => Promise<User>;
  getAll: () => Promise<User[]>;
  getByTeamId(teamId: string): Promise<User[]>;
}
