import { ProgrammingLanguage } from "@/models";

export interface ReadProgrammingLanguageService {
  get: (name: string) => Promise<ProgrammingLanguage>;
  getList: () => Promise<ProgrammingLanguage[]>;
}
