import { Team } from "@/models";

export interface ReadTeamService {
  get: (name: string) => Promise<Team>;
  getAll: () => Promise<Team[]>;
  getByProgrammingLanguageId(programmingLanguageId: string): Promise<Team[]>;
}
