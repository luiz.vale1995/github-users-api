import { User } from "@/models";
import { UserRepository } from "@/repository";
import { ReadUserService } from "./interfaces";

export class UserService implements ReadUserService {
  private userRepo: UserRepository;

  public constructor(userRepo: UserRepository) {
    this.userRepo = userRepo;
  }
  async get(id: string): Promise<User> {
    try {
      return await this.userRepo.findOne(id);
    } catch (e) {
      console.error(
        `Error in UserService method get with error message: ${e.message}`
      );
    }
  }
  async getAll(): Promise<User[]> {
    try {
      return await this.userRepo.find();
    } catch (e) {
      console.error(
        `Error in UserService method getList with error message: ${e.message}`
      );
    }
  }

  async getByTeamId(teamId: string): Promise<User[]> {
    try {
      return await this.userRepo.findByTeam(teamId);
    } catch (e) {
      console.error(
        `Error in UserService method getUserByTeam with error message: ${e.message}`
      );
    }
  }
}
