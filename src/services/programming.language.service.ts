import { ProgrammingLanguage } from "@/models";
import { ReadProgrammingLanguageRepository } from "@/repository/interfaces";
import { ReadProgrammingLanguageService } from "./interfaces";

export class ProgrammingLanguageService
  implements ReadProgrammingLanguageService
{
  private programmingLanguageRepository: ReadProgrammingLanguageRepository;
  constructor(
    programmingLanguageRepository: ReadProgrammingLanguageRepository
  ) {
    this.programmingLanguageRepository = programmingLanguageRepository;
  }
  async get(id: string): Promise<ProgrammingLanguage> {
    try {
      return await this.programmingLanguageRepository.findOne(id);
    } catch (e) {
      console.error(
        `Error in ProgrammingLanguageService, method get with error message: ${e.message}`
      );
    }
  }
  async getList(): Promise<ProgrammingLanguage[]> {
    try {
      return await this.programmingLanguageRepository.find();
    } catch (e) {
      console.error(
        `Error in ProgrammingLanguageService method getList with error message: ${e.message}`
      );
    }
  }
}
