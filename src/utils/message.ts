export default {
  InternalError: "Server cannot respond, please try later !",
  NotFound: "Could not found the requested resource !",
};
