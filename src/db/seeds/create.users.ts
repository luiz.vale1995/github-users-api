import "module-alias/register";
import env from "@/config/env";
import { UserEntity } from "../entities";
import fetch from "node-fetch";
import { DataSource } from "typeorm";

export default async function createUsers(
  dataSource: DataSource
): Promise<void> {
  const response = await fetch(env.githubUrl);
  const data: UserEntity[] = await response.json();
  await dataSource
    .createQueryBuilder()
    .insert()
    .into(UserEntity)
    .values(data)
    .execute();
}
