import DatabaseConfiguration from "../config/data.source";
import createLanguages from "./create.languages";
import createTeams from "./create.teams";
import createUsers from "./create.users";

DatabaseConfiguration.initialize().then((dataSource) => {
  createUsers(dataSource);
  createLanguages(dataSource);
  createTeams(dataSource);
  console.log("Database seeded !");
});
