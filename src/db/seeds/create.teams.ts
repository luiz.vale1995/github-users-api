import { DataSource } from "typeorm";
import { TeamEntity } from "../entities";

export default async function createTeams(
  dataSource: DataSource
): Promise<void> {
  const teams: TeamEntity[] = [
    "Management",
    "Project",
    "Marketing",
    "Task Force",
    "Performance",
    "Dissolution",
    "Training",
    "Storm",
  ].map((name) => {
    let entity = new TeamEntity();
    entity.name = name;
    return entity;
  });
  await dataSource
    .createQueryBuilder()
    .insert()
    .into(TeamEntity)
    .values(teams)
    .execute();
}
