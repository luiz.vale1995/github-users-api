import { DataSource } from "typeorm";
import { ProgrammingLanguageEntity } from "../entities";

export default async function createLanguages(
  dataSource: DataSource
): Promise<void> {
  const programmingLanguages: ProgrammingLanguageEntity[] = [
    "Typescript",
    "Golang",
    "Python",
    "Csharp",
    "C++",
    "Rust",
    "Elixir",
    "R",
  ].map((name) => {
    let entity = new ProgrammingLanguageEntity();
    entity.name = name;
    return entity;
  });
  await dataSource
    .createQueryBuilder()
    .insert()
    .into(ProgrammingLanguageEntity)
    .values(programmingLanguages)
    .execute();
}
