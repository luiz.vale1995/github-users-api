import { Column, Entity, JoinTable, ManyToMany } from "typeorm";
import BaseEntity from "./base.entity";
import { TeamEntity } from "./team.entity";
import { UserEntity } from "./user.entity";

@Entity({ name: "programming_language" })
export class ProgrammingLanguageEntity extends BaseEntity {
  @Column() name: string;
  @ManyToMany(() => UserEntity, (user) => user.programmingLanguages)
  users: UserEntity[];

  @ManyToMany(() => TeamEntity, (team) => team.programmingLanguages)
  @JoinTable()
  teams: TeamEntity[];
}
