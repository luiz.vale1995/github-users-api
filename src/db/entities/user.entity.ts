import { Column, Entity, JoinTable, ManyToMany } from "typeorm";
import BaseEntity from "./base.entity";
import { ProgrammingLanguageEntity } from "./programming.language.entity";
import { TeamEntity } from "./team.entity";

@Entity({ name: "user" })
export class UserEntity extends BaseEntity {
  @Column() login: string;
  @Column() node_id: string;
  @Column() avatar_url: string;
  @Column() gravatar_id: string;
  @Column() url: string;
  @Column() html_url: string;
  @Column() followers_url: string;
  @Column() following_url: string;
  @Column() gists_url: string;
  @Column() starred_url: string;
  @Column() subscriptions_url: string;
  @Column() organizations_url: string;
  @Column() repos_url: string;
  @Column() events_url: string;
  @Column() received_events_url: string;
  @Column() type: string;
  @Column() site_admin: false;
  @ManyToMany(
    () => ProgrammingLanguageEntity,
    (programmingLanguage) => programmingLanguage.users
  )
  @JoinTable()
  programmingLanguages: ProgrammingLanguageEntity[];

  @ManyToMany(() => TeamEntity, (team) => team.users)
  teams: TeamEntity[];
}
