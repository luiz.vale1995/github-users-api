import { Column, Entity, JoinTable, ManyToMany } from "typeorm";
import BaseEntity from "./base.entity";
import { ProgrammingLanguageEntity } from "./programming.language.entity";
import { UserEntity } from "./user.entity";

@Entity({ name: "team" })
export class TeamEntity extends BaseEntity {
  @Column() name: string;

  @ManyToMany(() => UserEntity, (user) => user.teams)
  @JoinTable()
  users: UserEntity[];

  @ManyToMany(
    () => ProgrammingLanguageEntity,
    (programmingLanguage) => programmingLanguage.teams
  )
  programmingLanguages: ProgrammingLanguageEntity[];
}
