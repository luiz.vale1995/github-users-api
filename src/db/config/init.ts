import DatabaseConfiguration from "./data.source";

DatabaseConfiguration.initialize()
    .then(() => {
        console.log("Database Inicializated !");
    })
    .catch((e) => console.log(e));
