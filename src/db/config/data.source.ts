import "module-alias/register";
import { envDb } from "@/config/env";
import { DataSource } from "typeorm";
import { UserEntity, TeamEntity, ProgrammingLanguageEntity } from "../entities";

export default new DataSource({
  type: "postgres",
  host: envDb.host,
  port: envDb.port,
  username: envDb.user,
  password: envDb.password,
  database: envDb.name,
  entities: [ProgrammingLanguageEntity, UserEntity, TeamEntity],
  synchronize: true,
  logging: false,
});
